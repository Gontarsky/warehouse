﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Warehouse.Domain.Abstract
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get();
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(int id);
        TEntity GetOne(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity enitity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}
