﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Warehouse.Domain.Entities
{
    public class VatInvoice
    {
        [Key]
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DateOfIssue { get; set; }
        public int ClientId { get; set; }

        public virtual Client Client { get; set; }
        public virtual ICollection<PositionVatInvoice> PositionsVatInvoice { get; set; }

        public decimal CalculateTotalNetValueOfVatInvoice()
        {
            return PositionsVatInvoice.Sum(p => p.NetProductValue);
        }

        public decimal CalculateTotalGrossValueOfVatInvoice()
        {
            return PositionsVatInvoice.Sum(p => p.NetProductValue * (1 + p.Vat));
        }

    }
}
