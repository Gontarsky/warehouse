﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Domain.Entities
{
    public class Address
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Miasto jest wymagane.")]
        [Display(Name = "Miasto")]
        public string City { get; set; }

        [Display(Name = "Ulica")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Numer mieszkania jest wymagany.")]
        [Display(Name = "Numer mieszkania")]
        public string LocalNumber { get; set; }

        [Required(ErrorMessage = "Kod pocztowy jest wymagany.")]
        [DataType(DataType.PostalCode)]
        [Display(Name = "Kod pocztowy")]
        public string Postcode { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<Delivery> Deliveries { get; set; }
    }
}
