﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Warehouse.Domain.Entities
{
    public enum Payment { Cash, Online, Undefined}
    public enum Status { Cancelled, OrderPlaced,Pending, Confirmed, Shipped, Delivered, Refund  }

    public class Order
    {
        [Key]
        [Display(Name="Nr.")]
        public int Id { get; set; }

        [DisplayName("Data zamówienia")]
        [Required]
        public DateTime OrderDate { get; set; }

        [Display(Name = "Wartość brutto")]
        [DataType(DataType.Currency)]
        [Required]
        public decimal GrossOrderValue { get; set; }

        [Display(Name = "Wartość netto")]
        [DataType(DataType.Currency)]
        [Required]

        public decimal NetOrderValue { get; set; }
        [Display(Name="Płatność")]
        [Required]
        public Payment Payment { get; set; }

        [Display(Name = "Status")]
        [Required]
        public Status Status { get; set; }


        [ForeignKey("Client")]
        public int ClientId { get; set; }
        [ForeignKey("Delivery")]
        public int? DeliveryId { get; set; }

        public virtual Client Client { get; set; }
        public virtual Delivery Delivery { get; set; }
        public virtual List<PositionOrder> PositionsOrder { get; set; }

        public void AddProductToOrder(Product product, int quantity)
        {
            PositionOrder positionOrder = null;
            if(PositionsOrder == null)
                PositionsOrder = new List<PositionOrder>();
            else
                positionOrder = PositionsOrder.SingleOrDefault(p => p.ProductId == product.Id);

            if (positionOrder == null)
            {
                PositionsOrder.Add(new PositionOrder
                {
                    Name = product.Name,
                    NetProductValue = product.NetProductValue,
                    Order = this,
                    OrderId = this.Id,
                    ProductId = product.Id,
                    Quantity = quantity,
                    Vat = product.Vat
                });
            }
            else
            {
                PositionsOrder.SingleOrDefault(p => p.ProductId == product.Id).Quantity += quantity;
            }
        }

        public void RemoveProductFromOrder(Product product)
        {
            PositionsOrder.RemoveAll(x => x.ProductId == product.Id);
        }

        public decimal CalculateNetValueOrder()
        {
            decimal netSum = 0;
            foreach (var position in PositionsOrder)
            {
                netSum += position.NetProductValue * position.Quantity;
            }

            return netSum;
        }

        public decimal CalculateGrossValueOrder()
        {
            decimal netSum = 0;
            foreach (var position in PositionsOrder)
            {
                netSum += position.Quantity * position.NetProductValue * (1 + position.Vat);
            }

            return netSum;
        }
    }

    
}
