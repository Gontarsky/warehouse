﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Warehouse.Domain.Entities
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("Imię")]
        [Required(ErrorMessage = "Imie jest wymagane.")]
        [MaxLength(20)]
        public string FirstName { get; set; }
        [DisplayName("Nazwisko")]
        [Required(ErrorMessage = "Nazwisko jest wymagane.")]
        [MaxLength(40)]
        public string LastName { get; set; }
        public string Nip { get; set; }
        public string Pesel { get; set; }

        [Required(ErrorMessage = "Adres Email jest wymagany do kontaktu.")]
        [EmailAddress(ErrorMessage = "Nieprawidłowa konstrukcja adresu Email. Przykładowy adres Email: bobkowalski@example.com.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Login jest wymagany.")]
        [DataType(DataType.Text)]
        [StringLength(50, ErrorMessage = "{0} musi mieć przynajmniej {2} znaków.", MinimumLength = 6)]
        public string Login { get; set; }

        [MaxLength(4000)]
        public string Hash { get; set; }
        [MaxLength(512)]
        public byte[] Salt { get; set; }

        [ForeignKey("Address")]
        public int? AddressId { get; set; }

        public virtual Address Address { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<VatInvoice> VatInvoices { get; set; }

        public IEnumerable<Order> GetOrdersWorthNetMoreThan(decimal value)
        {
            return Orders.Where(x => x.NetOrderValue > value);
        }
    }
}
