﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Domain.Entities
{
    public class PositionVatInvoice
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Nazwa")]
        [Required(ErrorMessage = "Nazwa pozycji faktury jest wymagana")]
        [MaxLength(50, ErrorMessage = "Maksymalna długość nazwy to 50 znaków")]
        public string Name { get; set; }
        [Display(Name = "Ilość produktu")]
        [Required(ErrorMessage = "Ilość produktu jest wymagana")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Zakres ilości produktu do ustawienia to [0,2147483647]")]
        public int Quantity { get; set; }
        [DataType(DataType.Currency)]
        [Display(Name = "Cena jednostkowa Netto")]
        [Required(ErrorMessage = "Cena Netto produktu jest wymagana")]
        public decimal NetProductValue { get; set; }
        [Display(Name = "Stawka VAT")]
        [Required(ErrorMessage = "Podatek od produktu jest wymagany")]
        [Range(0, 1, ErrorMessage = "Zakres możliwego podatku produktu do ustawienia to [0,1]")]
        public decimal Vat { get; set; }
        public int PositionOrderId { get; set; }
        public int VatInvoiceId { get; set; }
        public virtual PositionOrder PositionOrder { get; set; }
        public virtual VatInvoice VatInvoice { get; set; }
    }
}
