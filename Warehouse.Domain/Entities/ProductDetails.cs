﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Domain.Entities
{
    public class ProductDetails
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PathPhoto { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
    
        public virtual Product Product { get; set; }
    }
}
