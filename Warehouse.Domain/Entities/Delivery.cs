﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Domain.Entities
{
    public class Delivery
    {
        [Key]
        [Display(Name = "Nr dostawy")]
        public int Id { get; set; }
        [Display(Name = "Data wysłania")]
        public DateTime? ShippingDate { get; set; }
        [Display(Name = "Data dostarczenia")]
        public DateTime? DeliveryDate { get; set; }

        [MaxLength(100,ErrorMessage = "Maksymalna długość komentarza to 100 znaków")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Komentarz")]
        public string Comment { get; set; }

        [ForeignKey("DeliveryType")]
        public int DeliveryTypeId { get; set; }

        [ForeignKey("Address")]
        public int AddressId { get; set; }

        public virtual Address Address { get; set; }
        public virtual DeliveryType DeliveryType { get; set; }
    }
}
