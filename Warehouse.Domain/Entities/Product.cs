﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Domain.Entities
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nazwa")]
        [Required(ErrorMessage = "Nazwa produktu jest wymagana")]
        [MaxLength(50,ErrorMessage = "Maksymalna długość nazwy to 50 znaków")]
        public string Name { get; set; }

        [Display(Name = "Ilość produktu")]
        [Required(ErrorMessage = "Ilość produktu jest wymagana")]
        [Range(0,Int32.MaxValue,ErrorMessage = "Zakres ilości produktu do ustawienia to [{1},{2}]")]
        public int Quantity { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Cena jednostkowa Netto")]
        [Required(ErrorMessage = "Cena Netto produktu jest wymagana")]
        [Range(typeof(decimal),"0",
            "79228162514264337593543950335", ErrorMessage = "Zakres ceny Netto produktu to [{1},{2}]")]
        public decimal NetProductValue { get; set; }

        [Display(Name = "Stawka VAT")]
        [Required(ErrorMessage = "Podatek od produktu jest wymagany")]
        [Range(0,1,ErrorMessage = "Zakres możliwego podatku od produktu do ustawienia to [0,1]")]
        public decimal Vat { get; set; }

        [ForeignKey("Category")]
        public int? ProductCategoryId { get; set; }

        public virtual ProductCategory Category { get; set; }

        public virtual List<ProductDetails> ProductDetailses { get; set; }

        public decimal GetGrossProductValue()
        {
            return NetProductValue * (1 + Vat);
        }
    }
}
