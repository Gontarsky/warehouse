﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Domain.Entities
{
    public class PositionOrder
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Nazwa")]
        [Required(ErrorMessage = "Nazwa pozycji jest wymagana")]
        [MaxLength(50, ErrorMessage = "Maksymalna długość nazwy to 50 znaków")]
        public string Name { get; set; }

        [Display(Name = "Ilość produktu")]
        [Required(ErrorMessage = "Ilość produktu jest wymagana")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Zakres ilości produktu do ustawienia to [0,2147483647]")]
        public int Quantity { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Cena Netto")]
        [Required(ErrorMessage = "Cena Netto pozycji jest wymagana")]
        public decimal NetProductValue { get; set; }

        [Display(Name = "Stawka VAT")]
        [Required(ErrorMessage = "Podatek od produktów jest wymagany")]
        [Range(0, 1, ErrorMessage = "Zakres możliwego podatku od produktów do ustawienia to [0,1]")]
        public decimal Vat { get; set; }

        public int ProductId { get; set; }
        public int OrderId { get; set; }

        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }

        public decimal GetGrossProductValue()
        {
            return NetProductValue * (1 + Vat);
        }
    }
}
