﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Domain.Entities
{
    public class ProductCategory
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Nazwa kategorii jest wymagana.")]
        [Display(Name = "Kategoria")]
        [MaxLength(50,ErrorMessage = "Długość nazwy kategorii jest zbyt długa. Maksymalna ilość znaków to 50.")]
        public string CategoryName { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
