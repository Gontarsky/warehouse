﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Warehouse.Domain.Entities
{
    public class DeliveryType
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        [Display(Name = "Rodzaj przesyłki")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Koszt przesyłki jest obowiązkowy.")]
        [Display(Name = "Koszt przesyłki")]
        public decimal CostOfDeliveryType { get; set; }
        [Required(ErrorMessage = "Szacowany czas dostarczenia jest obowiązkowy.")]
        [Display(Name = "Szacowany czas dostarczenia")]
        [MaxLength(10,ErrorMessage = "Maksymalna długość szacowanego czasu dostarczenia to 10 znakow. Określamy w dniach!")]
        public string AverageTimeDelivery { get; set; }
        [Required(ErrorMessage = "Rodzaj płatności jest obowiązkowy.")]
        [Display(Name = "Rodzaj płatności")]
        public Payment Payment { get; set; }

        [NotMapped]
        public string FullInfo
        {
            get { return GetInfo(); }
        }

        public string GetInfo()
        {
            return Name + " - " + CostOfDeliveryType.ToString("C");
        }

    }
}
