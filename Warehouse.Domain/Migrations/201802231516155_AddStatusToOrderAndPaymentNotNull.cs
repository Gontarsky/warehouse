namespace Warehouse.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatusToOrderAndPaymentNotNull : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.Order", "Payment", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "Payment", c => c.Int());
            DropColumn("dbo.Order", "Status");
        }
    }
}
