namespace Warehouse.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStreetToAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Address", "Street", c => c.String());
            AlterColumn("dbo.Address", "City", c => c.String(nullable: false));
            AlterColumn("dbo.Address", "LocalNumber", c => c.String(nullable: false));
            AlterColumn("dbo.Address", "Postcode", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Address", "Postcode", c => c.String());
            AlterColumn("dbo.Address", "LocalNumber", c => c.String());
            AlterColumn("dbo.Address", "City", c => c.String());
            DropColumn("dbo.Address", "Street");
        }
    }
}
