namespace Warehouse.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ComplexDomainOfFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Client", "FirstName", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Client", "LastName", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.DeliveryType", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.DeliveryType", "AverageTimeDelivery", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.PositionOrder", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Product", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.PositionVatInvoice", "Name", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PositionVatInvoice", "Name", c => c.String());
            AlterColumn("dbo.Product", "Name", c => c.String());
            AlterColumn("dbo.PositionOrder", "Name", c => c.String());
            AlterColumn("dbo.DeliveryType", "AverageTimeDelivery", c => c.String(nullable: false));
            AlterColumn("dbo.DeliveryType", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Client", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.Client", "FirstName", c => c.String(nullable: false));
        }
    }
}
