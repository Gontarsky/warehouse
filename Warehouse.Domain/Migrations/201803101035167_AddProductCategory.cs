namespace Warehouse.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProductCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCategory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Product", "ProductCategoryId", c => c.Int());
            CreateIndex("dbo.Product", "ProductCategoryId");
            AddForeignKey("dbo.Product", "ProductCategoryId", "dbo.ProductCategory", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Product", "ProductCategoryId", "dbo.ProductCategory");
            DropIndex("dbo.Product", new[] { "ProductCategoryId" });
            DropColumn("dbo.Product", "ProductCategoryId");
            DropTable("dbo.ProductCategory");
        }
    }
}
