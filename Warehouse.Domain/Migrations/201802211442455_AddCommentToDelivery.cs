namespace Warehouse.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentToDelivery : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Delivery", "Comment", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Delivery", "Comment");
        }
    }
}
