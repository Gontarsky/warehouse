namespace Warehouse.Domain.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddNullableDeliveryId : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Address",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        City = c.String(),
                        LocalNumber = c.String(),
                        Postcode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Nip = c.String(),
                        Pesel = c.String(),
                        Email = c.String(nullable: false),
                        Login = c.String(nullable: false, maxLength: 50),
                        Hash = c.String(maxLength: 4000),
                        Salt = c.Binary(maxLength: 512),
                        AddressId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        GrossOrderValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NetOrderValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Payment = c.Int(),
                        ClientId = c.Int(nullable: false),
                        DeliveryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Delivery", t => t.DeliveryId)
                .Index(t => t.ClientId)
                .Index(t => t.DeliveryId);
            
            CreateTable(
                "dbo.Delivery",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ShippingDate = c.DateTime(),
                        DeliveryDate = c.DateTime(),
                        DeliveryTypeId = c.Int(nullable: false),
                        AddressId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Address", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.DeliveryType", t => t.DeliveryTypeId, cascadeDelete: true)
                .Index(t => t.DeliveryTypeId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.DeliveryType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        CostOfDeliveryType = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AverageTimeDelivery = c.String(nullable: false),
                        Payment = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PositionOrder",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Quantity = c.Int(nullable: false),
                        NetProductValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProductId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Order", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Quantity = c.Int(nullable: false),
                        NetProductValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VatInvoice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InvoiceNumber = c.String(),
                        DateOfIssue = c.DateTime(nullable: false),
                        ClientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Client", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.PositionVatInvoice",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Quantity = c.Int(nullable: false),
                        NetProductValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PositionOrderId = c.Int(nullable: false),
                        VatInvoiceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PositionOrder", t => t.PositionOrderId)
                .ForeignKey("dbo.VatInvoice", t => t.VatInvoiceId, cascadeDelete: true)
                .Index(t => t.PositionOrderId)
                .Index(t => t.VatInvoiceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PositionVatInvoice", "VatInvoiceId", "dbo.VatInvoice");
            DropForeignKey("dbo.PositionVatInvoice", "PositionOrderId", "dbo.PositionOrder");
            DropForeignKey("dbo.VatInvoice", "ClientId", "dbo.Client");
            DropForeignKey("dbo.PositionOrder", "ProductId", "dbo.Product");
            DropForeignKey("dbo.PositionOrder", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "DeliveryId", "dbo.Delivery");
            DropForeignKey("dbo.Delivery", "DeliveryTypeId", "dbo.DeliveryType");
            DropForeignKey("dbo.Delivery", "AddressId", "dbo.Address");
            DropForeignKey("dbo.Order", "ClientId", "dbo.Client");
            DropForeignKey("dbo.Client", "AddressId", "dbo.Address");
            DropIndex("dbo.PositionVatInvoice", new[] { "VatInvoiceId" });
            DropIndex("dbo.PositionVatInvoice", new[] { "PositionOrderId" });
            DropIndex("dbo.VatInvoice", new[] { "ClientId" });
            DropIndex("dbo.PositionOrder", new[] { "OrderId" });
            DropIndex("dbo.PositionOrder", new[] { "ProductId" });
            DropIndex("dbo.Delivery", new[] { "AddressId" });
            DropIndex("dbo.Delivery", new[] { "DeliveryTypeId" });
            DropIndex("dbo.Order", new[] { "DeliveryId" });
            DropIndex("dbo.Order", new[] { "ClientId" });
            DropIndex("dbo.Client", new[] { "AddressId" });
            DropTable("dbo.PositionVatInvoice");
            DropTable("dbo.VatInvoice");
            DropTable("dbo.Product");
            DropTable("dbo.PositionOrder");
            DropTable("dbo.DeliveryType");
            DropTable("dbo.Delivery");
            DropTable("dbo.Order");
            DropTable("dbo.Client");
            DropTable("dbo.Address");
        }
    }
}
