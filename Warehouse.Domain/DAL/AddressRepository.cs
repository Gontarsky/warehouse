﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class AddressRepository : IRepository<Address>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<Address> Get()
        {
            return database.Addresses;
        }

        public IQueryable<Address> Get(Expression<Func<Address, bool>> predicate)
        {
            if (predicate == null)
                return database.Addresses;
            return database.Addresses.Where(predicate);
        }

        public Address Get(int id)
        {
            return database.Addresses.SingleOrDefault(x => x.Id == id);
        }

        public Address GetOne(Expression<Func<Address, bool>> predicate)
        {
            return database.Addresses.SingleOrDefault(predicate);
        }

        public void Add(Address enitity)
        {
            database.Addresses.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(Address entity)
        {
            database.Addresses.Remove(entity);
            database.SaveChanges();
        }

        public void Update(Address entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }

    }
}
