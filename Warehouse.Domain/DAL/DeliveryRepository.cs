﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class DeliveryRepository : IRepository<Delivery>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<Delivery> Get()
        {
            return database.Deliveries;
        }

        public IQueryable<Delivery> Get(Expression<Func<Delivery, bool>> predicate)
        {
            if (predicate == null)
                return database.Deliveries;
            return database.Deliveries.Where(predicate);
        }

        public Delivery Get(int id)
        {
            return database.Deliveries.SingleOrDefault(x => x.Id == id);
        }

        public Delivery GetOne(Expression<Func<Delivery, bool>> predicate)
        {
            return database.Deliveries.SingleOrDefault(predicate);
        }

        public void Add(Delivery enitity)
        {
            database.Deliveries.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(Delivery entity)
        {
            database.Deliveries.Remove(entity);
            database.SaveChanges();
        }

        public void Update(Delivery entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
