﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class ProductCategoryRepository :IRepository<ProductCategory>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<ProductCategory> Get()
        {
            return database.ProductsCategory;
        }

        public IQueryable<ProductCategory> Get(Expression<Func<ProductCategory, bool>> predicate)
        {
            if (predicate == null)
                return database.ProductsCategory;
            return database.ProductsCategory.Where(predicate);
        }

        public ProductCategory Get(int id)
        {
            return database.ProductsCategory.SingleOrDefault(x => x.Id == id);
        }

        public ProductCategory GetOne(Expression<Func<ProductCategory, bool>> predicate)
        {
            return database.ProductsCategory.SingleOrDefault(predicate);
        }

        public void Add(ProductCategory enitity)
        {
            database.ProductsCategory.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(ProductCategory entity)
        {
            database.ProductsCategory.Remove(entity);
            database.SaveChanges();
        }

        public void Update(ProductCategory entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
