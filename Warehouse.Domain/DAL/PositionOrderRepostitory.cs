﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class PositionOrderRepostitory : IRepository<PositionOrder>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<PositionOrder> Get()
        {
            return database.PositionsOrder;
        }

        public IQueryable<PositionOrder> Get(Expression<Func<PositionOrder, bool>> predicate)
        {
            if (predicate == null)
                return database.PositionsOrder;
            return database.PositionsOrder.Where(predicate);
        }

        public PositionOrder Get(int id)
        {
            return database.PositionsOrder.SingleOrDefault(x => x.Id == id);
        }

        public PositionOrder GetOne(Expression<Func<PositionOrder, bool>> predicate)
        {
            return database.PositionsOrder.SingleOrDefault(predicate);
        }

        public void Add(PositionOrder enitity)
        {
            database.PositionsOrder.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(PositionOrder entity)
        {
            database.PositionsOrder.Remove(entity);
            database.SaveChanges();
        }

        public void Update(PositionOrder entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
