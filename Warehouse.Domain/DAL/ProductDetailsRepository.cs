﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class ProductDetailsRepository : IRepository<ProductDetails>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<ProductDetails> Get()
        {
            return database.ProductDetailses;
        }

        public IQueryable<ProductDetails> Get(Expression<Func<ProductDetails, bool>> predicate)
        {
            if (predicate == null)
                return database.ProductDetailses;
            return database.ProductDetailses.Where(predicate);
        }

        public ProductDetails Get(int id)
        {
            return database.ProductDetailses.SingleOrDefault(x => x.Id == id);
        }

        public ProductDetails GetOne(Expression<Func<ProductDetails, bool>> predicate)
        {
            return database.ProductDetailses.SingleOrDefault(predicate);
        }

        public void Add(ProductDetails enitity)
        {
            database.ProductDetailses.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(ProductDetails entity)
        {
            database.ProductDetailses.Remove(entity);
            database.SaveChanges();
        }

        public void Update(ProductDetails entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
