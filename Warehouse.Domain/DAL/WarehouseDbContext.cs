﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class WarehouseDbContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Configure Code First to ignore PluralizingTableName convention 
            // If you keep this convention then the generated tables will have pluralized names. 
            modelBuilder.Entity<PositionVatInvoice>()
                .HasRequired(p => p.PositionOrder)
                .WithMany()
                .HasForeignKey(p => p.PositionOrderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasOptional(o => o.Delivery)
                .WithMany()
                .HasForeignKey(o => o.DeliveryId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PositionOrder>()
                .HasRequired(p => p.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId)
                .WillCascadeOnDelete(false);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<VatInvoice> VatInvoices { get; set; }
        public DbSet<PositionOrder> PositionsOrder { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PositionVatInvoice> PositionsVatInvoice { get; set; }
        public DbSet<DeliveryType> DeliveryTypes { get; set; }
        public DbSet<ProductDetails> ProductDetailses { get; set; }
        public DbSet<ProductCategory> ProductsCategory { get; set; }
    }
}
