﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class VatInvoiceRepository : IRepository<VatInvoice>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<VatInvoice> Get()
        {
            return database.VatInvoices;
        }

        public IQueryable<VatInvoice> Get(Expression<Func<VatInvoice, bool>> predicate)
        {
            if (predicate == null)
                return database.VatInvoices;
            return database.VatInvoices.Where(predicate);
        }

        public VatInvoice Get(int id)
        {
            return database.VatInvoices.SingleOrDefault(x => x.Id == id);
        }

        public VatInvoice GetOne(Expression<Func<VatInvoice, bool>> predicate)
        {
            return database.VatInvoices.SingleOrDefault(predicate);
        }

        public void Add(VatInvoice enitity)
        {
            database.VatInvoices.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(VatInvoice entity)
        {
            database.VatInvoices.Remove(entity);
            database.SaveChanges();
        }

        public void Update(VatInvoice entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
