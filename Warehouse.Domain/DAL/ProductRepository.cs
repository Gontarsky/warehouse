﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class ProductRepository : IRepository<Product>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<Product> Get()
        {
            return database.Products;
        }

        public IQueryable<Product> Get(Expression<Func<Product, bool>> predicate)
        {
            if (predicate == null)
                return database.Products;
            return database.Products.Where(predicate);
        }

        public Product Get(int id)
        {
            return database.Products.SingleOrDefault(x => x.Id == id);
        }

        public Product GetOne(Expression<Func<Product, bool>> predicate)
        {
            return database.Products.SingleOrDefault(predicate);
        }

        public void Add(Product enitity)
        {
            database.Products.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(Product entity)
        {
            database.Products.Remove(entity);
            database.SaveChanges();
        }

        public void Update(Product entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
