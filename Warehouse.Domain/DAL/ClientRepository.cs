﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class ClientRepository : IRepository<Client>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<Client> Get()
        {
            return database.Clients;
        }

        public IQueryable<Client> Get(Expression<Func<Client, bool>> predicate)
        {
            if (predicate == null)
                return database.Clients;
            return database.Clients.Where(predicate);
        }

        public Client Get(int id)
        {
            return database.Clients.SingleOrDefault(x => x.Id == id);
        }

        public Client GetOne(Expression<Func<Client, bool>> predicate)
        {
            return database.Clients.SingleOrDefault(predicate);
        }

        public void Add(Client enitity)
        {
            database.Clients.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(Client entity)
        {
            database.Clients.Remove(entity);
            database.SaveChanges();
        }

        public void Update(Client entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
