﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class DeliveryTypeRepository : IRepository<DeliveryType>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<DeliveryType> Get()
        {
            return database.DeliveryTypes;
        }

        public IQueryable<DeliveryType> Get(Expression<Func<DeliveryType, bool>> predicate)
        {
            if (predicate == null)
                return database.DeliveryTypes;
            return database.DeliveryTypes.Where(predicate);
        }

        public DeliveryType Get(int id)
        {
            return database.DeliveryTypes.SingleOrDefault(x => x.Id == id);
        }

        public DeliveryType GetOne(Expression<Func<DeliveryType, bool>> predicate)
        {
            return database.DeliveryTypes.SingleOrDefault(predicate);
        }

        public void Add(DeliveryType enitity)
        {
            database.DeliveryTypes.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(DeliveryType entity)
        {
            database.DeliveryTypes.Remove(entity);
            database.SaveChanges();
        }

        public void Update(DeliveryType entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
