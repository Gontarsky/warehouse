﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class OrderRepository : IRepository<Order>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<Order> Get()
        {
            return database.Orders;
        }

        public IQueryable<Order> Get(Expression<Func<Order, bool>> predicate)
        {
            if (predicate == null)
                return database.Orders;
            return database.Orders.Where(predicate);
        }

        public Order Get(int id)
        {
            return database.Orders.SingleOrDefault(x => x.Id == id);
        }

        public Order GetOne(Expression<Func<Order, bool>> predicate)
        {
            return database.Orders.SingleOrDefault(predicate);
        }

        public void Add(Order enitity)
        {
            database.Orders.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(Order entity)
        {
            database.Orders.Remove(entity);
            database.SaveChanges();
        }

        public void Update(Order entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
