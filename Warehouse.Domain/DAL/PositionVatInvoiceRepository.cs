﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.Domain.DAL
{
    public class PositionVatInvoiceRepository : IRepository<PositionVatInvoice>
    {
        private WarehouseDbContext database = new WarehouseDbContext();

        public IEnumerable<PositionVatInvoice> Get()
        {
            return database.PositionsVatInvoice;
        }

        public IQueryable<PositionVatInvoice> Get(Expression<Func<PositionVatInvoice, bool>> predicate)
        {
            if (predicate == null)
                return database.PositionsVatInvoice;
            return database.PositionsVatInvoice.Where(predicate);
        }

        public PositionVatInvoice Get(int id)
        {
            return database.PositionsVatInvoice.SingleOrDefault(x => x.Id == id);
        }

        public PositionVatInvoice GetOne(Expression<Func<PositionVatInvoice, bool>> predicate)
        {
            return database.PositionsVatInvoice.SingleOrDefault(predicate);
        }

        public void Add(PositionVatInvoice enitity)
        {
            database.PositionsVatInvoice.Add(enitity);
            database.SaveChanges();
        }

        public void Delete(PositionVatInvoice entity)
        {
            database.PositionsVatInvoice.Remove(entity);
            database.SaveChanges();
        }

        public void Update(PositionVatInvoice entity)
        {
            database.Entry(entity).State = EntityState.Modified;
            database.SaveChanges();
        }
    }
}
