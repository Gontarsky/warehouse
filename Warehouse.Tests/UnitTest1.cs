﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Controllers;
using Warehouse.WebUI.Models;
using Warehouse.WebUI.Security;

namespace Warehouse.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Calculate_Total_Net_Value_Of_Vat_Income()
        {
            // Arrange
            ICollection<PositionVatInvoice> positionsVat = new List<PositionVatInvoice>
            {
                new PositionVatInvoice{NetProductValue = 3500},
                new PositionVatInvoice{NetProductValue = 5400},
                new PositionVatInvoice{NetProductValue = 1000},
                new PositionVatInvoice{NetProductValue = 2000},
                new PositionVatInvoice{NetProductValue = 5000},
                new PositionVatInvoice{NetProductValue = 500},
                new PositionVatInvoice{NetProductValue = 100}
            };
            VatInvoice vat = new VatInvoice{PositionsVatInvoice = positionsVat};

            // Act
            decimal result = vat.CalculateTotalNetValueOfVatInvoice();

            // Assert
            Assert.AreEqual(result,17500M);
        }

        [TestMethod]
        public void Can_Calculate_Total_Gross_Value_Of_Vat_Income()
        {
            // Arrange
            ICollection<PositionVatInvoice> positionsVat = new List<PositionVatInvoice>
            {
                new PositionVatInvoice{NetProductValue = 3000, Vat = 0.23M},
                new PositionVatInvoice{NetProductValue = 5000, Vat = 0.23M},
                new PositionVatInvoice{NetProductValue = 1000, Vat = 0.23M},
                new PositionVatInvoice{NetProductValue = 2000, Vat = 0.23M},
                new PositionVatInvoice{NetProductValue = 5000, Vat = 0.23M}

            };
            VatInvoice vat = new VatInvoice { PositionsVatInvoice = positionsVat };

            // Act
            decimal result = vat.CalculateTotalGrossValueOfVatInvoice();

            // Assert
            Assert.AreEqual(result, 19680M);
        }

        [TestMethod]
        public void Can_Add_Postion_To_Order()
        {
            // Arrange
            Product product = new Product {Id = 1, Name = "Młotek", NetProductValue = 150M, Quantity = 15, Vat = 0.23M};
            List<PositionOrder> positions = new List<PositionOrder>();
            Order order = new Order{Id = 1,PositionsOrder = positions};

            // Act
            order.AddProductToOrder(product,5);
            order.AddProductToOrder(product,5);

            // Assert
            Assert.AreEqual(order.PositionsOrder.ToList()[0].Quantity,10);
        }

        [TestMethod]
        public void Can_Login_To_Warehouse()
        {
            // Arrange
            string login = "stasiek";
            string password = "krotołamacz";
            string returnUrl = "/Client/Index";
            byte[] salt = Hasher.Get_SALT();
            string hash = Hasher.Get_HASH_SHA512(password, login, salt);
            Mock<IRepository<Client>> mock = new Mock<IRepository<Client>>();
            mock.Setup(x => x.Get()).Returns(new[]
            {
                new Client {Login = login, Salt = salt, Hash = hash}
            });

            Mock<HttpContextBase> context = new Mock<HttpContextBase>();
            ClientController controller = new ClientController(mock.Object)
            {
                Url = new UrlHelper(
                    new RequestContext(context.Object, new RouteData()),
                    new RouteCollection()
                )
            };


            // Act
            RedirectResult result = controller.Login(new ClientVML {Login = login, Password = password, ReturnUrl = returnUrl}) as RedirectResult;

            // Assert
            Assert.AreEqual(result.Url, returnUrl);
        }
    }
}
