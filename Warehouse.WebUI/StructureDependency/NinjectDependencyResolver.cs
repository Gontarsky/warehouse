﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Moq;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.DAL;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Abstract;
using Warehouse.WebUI.Services.Email;

namespace Warehouse.WebUI.StructureDependency
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            this.kernel = kernel;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        public void AddBindings()
        {
            // Here I will add dependency between interfaces and their specific implementations
            kernel.Bind<IRepository<Client>>().To<ClientRepository>();
            kernel.Bind<IRepository<Product>>().To<ProductRepository>();
            kernel.Bind<IRepository<Order>>().To<OrderRepository>();
            kernel.Bind<IRepository<Delivery>>().To<DeliveryRepository>();
            kernel.Bind<IRepository<DeliveryType>>().To<DeliveryTypeRepository>();
            kernel.Bind<IRepository<Address>>().To<AddressRepository>();
            kernel.Bind<IRepository<ProductDetails>>().To<ProductDetailsRepository>();
            kernel.Bind<IRepository<ProductCategory>>().To<ProductCategoryRepository>();
            kernel.Bind<IEmailSender>().To<EmailService>();
        }
    }
}