﻿using Warehouse.WebUI.Models;

namespace Warehouse.WebUI.Abstract
{
    public interface IEmailSender
    {
        void SendEmail(EmailModel emailModel);
        void SendEmailForContact(EmailModel emailModel);
    }
}