﻿using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Abstract
{
    interface IProcessOrder
    {
        void ProcessOrder(Order order, int idClient);
    }
}
