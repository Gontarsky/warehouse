﻿using System;
using System.Net;
using System.Web.Mvc;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Models;
using Warehouse.WebUI.Security;
using Warehouse.WebUI.Services.Order;

namespace Warehouse.WebUI.Controllers
{
    [Authorize]
    [SessionTimeout]
    public class OrderController : Controller
    {
        private PagginationService _pagginationService;
        private OrderService _orderService;

        public OrderController(IRepository<Order> orderRepository, IRepository<Product> productRepository)
        {
            _pagginationService = new PagginationService(orderRepository);
            _orderService = new OrderService(orderRepository,productRepository);
        }

        public ViewResult Checkout(Order order, string returnUrl)
        {
            if(CheckIfCheckoutIsEmpty(order))
                ViewData["EmptyCheckout"] = "Twój koszyk jest pusty!";

            return View(new OrderIndexViewModel
            {
                Order = order,
                ReturnUrl = returnUrl
            });
        }

        [HttpGet]
        public ActionResult List(int page = 1, int pageSize = 10)
        {
            if (Session["ClientId"] == null)
                return RedirectToAction("Login", "Client");

            var idClient = Int32.Parse(Session["ClientId"].ToString());
            var model = _pagginationService
                .GetPagginationModelForClient(idClient, page, pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if( id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Order order = _orderService.GetOrder(id.Value);
            if(order != null)
                return View(order.PositionsOrder);

            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Order order = _orderService.GetOrder(id.Value);
            if (order != null)
                _orderService.DeleteOrderFromDb(order);

            return RedirectToAction("List");
        }


        [HttpPost]
        public RedirectToRouteResult AddToOrder(Order order, int Id, string returnUrl)
        {
            Product product = _orderService.GetProduct(Id);

            if(product != null)
                order.AddProductToOrder(product,1);

            return RedirectToAction("Checkout", new {returnUrl});
        }

        [HttpPost]
        public RedirectToRouteResult RemoveFromOrder(Order order, int productId, string returnUrl)
        {
            Product product = _orderService.GetProduct(productId);

            if (product != null)
                order.RemoveProductFromOrder(product);

            if (CheckIfCheckoutIsEmpty(order))
                ViewData["EmptyCheckout"] = "Twój koszyk jest pusty!";

            return RedirectToAction("Checkout", new { returnUrl });
        }

        [HttpPost]
        public RedirectToRouteResult MakeOrder(Order order)
        {
            if (Session["ClientId"] == null)
                return RedirectToAction("Login", "Client");

            var idClient = Int32.Parse(Session["ClientId"].ToString());
            _orderService.InitializeOrder(idClient, order);

            _orderService.AddOrderToDb(order);
            int orderId = order.Id;
            Session["Order"] = null;

            return RedirectToAction("ChooseDelivery", "Delivery", new {idOrder = orderId });
        }

        private bool CheckIfCheckoutIsEmpty(Order order)
        {
            return order.PositionsOrder == null || order.PositionsOrder.Count == 0;
        }



    }
}