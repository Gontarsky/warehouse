﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Models.Pagination;
using Warehouse.WebUI.Security;
using Warehouse.WebUI.Services.Product;

namespace Warehouse.WebUI.Controllers
{
    public class ProductController : Controller
    {
        private PagginationService _pagginationService;
        private ProductService _productService;

        public ProductController(IRepository<Product> productRepository, IRepository<ProductDetails> productDetails)
        {
            _pagginationService = new PagginationService(productRepository);
            _productService = new ProductService(productRepository,productDetails);
        }

        [HttpGet]
        [Authorize]
        public ActionResult SearchProductsForOrder(string phrase)
        {
            var model = _pagginationService.GetSearchedModel(1, 10, phrase);
            return View("ShowProducts", model);
        }

        [HttpGet]
        public ActionResult SearchProducts(string phrase)
        {
            var model = _pagginationService.GetSearchedModel(1, 10, phrase);
            return View("List", model);
        }

        [HttpGet]
        public ActionResult List(string category,int page = 1, int pageSize = 10)
        {
            EntitiesListViewModel<Product> model = _pagginationService.GetPagginationModel(page, pageSize, category);
            return View(model);
        }

        [HttpGet]
        [SessionTimeout]
        [Authorize]
        public ActionResult AddProduct()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        [SessionTimeout]
        public ActionResult AddProduct(Product product, HttpPostedFileBase file)
        {
            ValidateFile(file);
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    AddProductWithImage(product,file);
                    ShowCorrectMessage();
                }
                else
                {
                    _productService.AddProductToDb(product);
                    ShowCorrectMessage();
                }
                
            }
            return View();
        }

        [HttpGet]
        public ActionResult ProductInfo(int id)
        {
            Product product =  _productService.GetProduct(id);
            if (!CheckCorrectnessProduct(product))
                return RedirectToAction("ShowProducts");
                
            return View(product);
        }

        [HttpGet]
        [Authorize]
        [SessionTimeout]
        public ActionResult ShowProducts(string category,int page = 1, int pageSize = 10)
        {
            EntitiesListViewModel<Product> model =
                _pagginationService.GetPagginationModel(page, pageSize, category);
            _productService.ConnectProductsWithImages(model.Entities);
            return View(model);
        }

        private void AddProductWithImage(Product product, HttpPostedFileBase file)
        {
            _productService.AddProductToDb(product);
            string pathGallery = $"~\\ProductPhotos\\{product.Id}\\";
            CreatePathIfNotExist(pathGallery);

            _productService.SaveFile(file, Path.Combine(Server.MapPath(pathGallery)));
            var productDetails =
                _productService.GetProductDetails(product.Id, pathGallery.Remove(0, 1) + file.FileName);
            _productService.AddProductDetailsToDb(productDetails);
        }

        private void CreatePathIfNotExist(string path) // care about permission
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(Server.MapPath(path));
        }

        private void ValidateFile(HttpPostedFileBase file)
        {
            if (!_productService.ValidateTypeFile(file))
            {
                ModelState.AddModelError("WrongExtesionOfFile",
                    @"Nie obsługiwany typ pliku! Pliki graficzne są tylko możliwe do przesłania");
            }
            if (!_productService.ValidateSizeFile(file))
            {
                ViewBag.MaxSizeFilekB = _productService.MaxSizeOfPictureInkB;
                ModelState.AddModelError("WrongSizeOfFile",
                    $@"Maksymalny dopuszalny rozmiar pliku to {(_productService.MaxSizeOfPictureInMB):##.###}MB");
            }
        }

        private bool CheckCorrectnessProduct(Product product)
        {
            if (product == null)
            {
                TempData["ProductIsNull"] = "Taki produkt nie istnieje!";
                return false;
            }

            return true;
        }

        private void ShowCorrectMessage()
        {
            ModelState.Clear();
            ViewBag.MessageOKAddProduct = "Produkt został zapisany!";
        }
    }
}