﻿using System.Web.Mvc;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Services.ProductCategory;

namespace Warehouse.WebUI.Controllers
{
    public class ProductCategoryController : Controller
    {
        private ProductCategoryService _productCategoryService;

        public ProductCategoryController(IRepository<ProductCategory> productCategoryRepository)
        {
            _productCategoryService = new ProductCategoryService(productCategoryRepository);
        }

        public PartialViewResult Menu(string category = null)
        {
            ViewBag.SelectedCategory = category;
            var categories = _productCategoryService.GetProductCategories();
            return PartialView(categories);
        }
    }
}