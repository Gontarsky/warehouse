﻿using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Security;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Models;
using Warehouse.WebUI.Security;
using Warehouse.WebUI.Services.Client;


namespace Warehouse.WebUI.Controllers
{
    public class ClientController : Controller
    {
        private ClientService _clientService;

        public ClientController(IRepository<Client> clientsRepository)
        {
            _clientService = new ClientService(clientsRepository);
        }

        [HttpGet]
        [Authorize]
        [SessionTimeout]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CompletedRegistration()
        {
            if(TempData["ReturnUrl"] == null || !TempData["ReturnUrl"].ToString().Contains("Register"))
                return RedirectToAction("Index");
            return View();   
        }

        [HttpGet]
        public ActionResult Register()
        {
            if(!Request.IsAuthenticated)
                return View();

            return RedirectToAction("Index");
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(ClientVMR user)
        {
            ValidateClient(user.Client);

            if (ModelState.IsValid)
            {
                Client client = _clientService.HashAndSaltClient(user);
                _clientService.AddClientToDb(client);
                TempData["ReturnUrl"] = user.ReturnUrl;
                return RedirectToAction("CompletedRegistration");
            }

            return View("Register");
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            EnsureLoggedOut();

            var clientInfo = new ClientVML {ReturnUrl = returnUrl};

            return View(clientInfo);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(ClientVML clientVML)
        {
            if (!ModelState.IsValid)
            {
                return View(clientVML);
            }

            if (_clientService.ValidateLogging(clientVML))
            {
                SignInRemember(clientVML.Login,clientVML.IsRemember);
                Session["ClientId"] = _clientService.GetId(clientVML.Login);

                return RedirectToLocal(clientVML.ReturnUrl);
            }

            TempData["LoginError"] = "Nieprawidłowy login lub hasło. Spróbuj ponownie.";
            return View(clientVML);
        }


        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);

            Session.Clear();
            System.Web.HttpContext.Current.Session.RemoveAll();

            return View("Index");
        }

        [HttpGet]
        public ActionResult SessionExpired()
        {
            return View();
        }

        private void SignInRemember(string login, bool isPersistent = false)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.SetAuthCookie(login,isPersistent);
        }

        private void ValidateClient(Client client)
        {
            if (!_clientService.ValidateLogin(client.Login))
            {
                ModelState.AddModelError("Client.Login", @"Login jest już zajęty!");
            }
            if (!_clientService.ValidateLogin(client.Login))
            {
                ModelState.AddModelError("Client.Email", @"Email jest już zajęty!");
            }
            if (!_clientService.ValidatePesel(client.Pesel))
            {
                ModelState.AddModelError("Client.Pesel", @"Pesel jest już zajęty!");
            }

            if (!_clientService.ValidateNip(client.Nip))
            {
                ModelState.AddModelError("Nip", @"Nip jest już zajęty!");
            }
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            if (!string.IsNullOrWhiteSpace(returnUrl) && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Client");
        }

        private void EnsureLoggedOut()
        {
            if (Request.IsAuthenticated)
                Logout();
        }


    }
}