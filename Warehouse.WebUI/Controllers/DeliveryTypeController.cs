﻿using System.Linq;
using System.Web.Mvc;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Controllers
{
    public class DeliveryTypeController : Controller
    {
        private IRepository<DeliveryType> _deliveryRepository;

        public DeliveryTypeController(IRepository<DeliveryType> deliveryRepository)
        {
            _deliveryRepository = deliveryRepository;
        }

        [HttpGet]
        public ActionResult InformationAboutDelivery()
        {
            var deliveryTypes = _deliveryRepository.Get().ToList();
            return View(deliveryTypes);
        }
    }
}