﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Abstract;
using Warehouse.WebUI.Models;
using Warehouse.WebUI.Services.Home;

namespace Warehouse.WebUI.Controllers
{
    public class HomeController : Controller
    {
        private IEmailSender _emailSender;
        private HomeService _homeService;

        public HomeController(IEmailSender emailSender, IRepository<Client> clientRepository)
        {
            _emailSender = emailSender;
            _homeService = new HomeService(clientRepository);
        }

        [HttpGet]
        [Route(Name = "Contact")]
        public ActionResult SendEmailForContact()
        {
            if (Request.IsAuthenticated && Session["ClientId"] != null)
            {
                var emailModelForClient =
                    _homeService.GetEmailModelForClient((int) Session["ClientId"]);
                return View("SendEmailForContactAuthorized", emailModelForClient);
            }
            return View(new EmailModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendEmailForContact(EmailModel emailModel)
        {
            _emailSender.SendEmailForContact(emailModel);
            return RedirectToAction("List", "Order");
        }
    }
}