﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Models;
using Warehouse.WebUI.Security;
using Warehouse.WebUI.Services.Delivery;

namespace Warehouse.WebUI.Controllers
{
    [Authorize]
    [SessionTimeout]
    public class DeliveryController : Controller
    {
        private readonly DeliveryService _deliveryService;

        public DeliveryController(IRepository<Delivery> deliveryRepository, IRepository<Order> orderRepository, IRepository<DeliveryType> deliveryTypeRepository, IRepository<Address> addressRepository)
        {
            _deliveryService = new DeliveryService(deliveryRepository,orderRepository,deliveryTypeRepository,addressRepository);
        }

        [HttpGet]
        public ActionResult ChooseDelivery(int idOrder)
        {
            if (_deliveryService.CheckIfWasDone(idOrder))
                return RedirectToAction("List", "Order");

            ViewBag.DeliveryTypes = 
                GetDeliveryTypesList(_deliveryService.GetTypesOfDelivery());
            var model = _deliveryService.GetDeliveryAddressVM(idOrder, "SelectedType");
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetDelivery(DeliveryAddressViewModel deliveryAddress)
        {
            if (!ModelState.IsValid)
                return View("ChooseDelivery");

            AddDelivery(deliveryAddress);
            return RedirectToAction("List", "Order");
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (_deliveryService.CheckIfOrderHasDelivery(id.Value))
                return View(_deliveryService.GetDeliveryTypeVM(id.Value));

            return RedirectToAction("List","Order");
        }

        private void AddDelivery(DeliveryAddressViewModel deliveryAddress)
        {
            var delivery = deliveryAddress.Delivery;
            delivery.DeliveryTypeId = Int32.Parse(deliveryAddress.SelectedType);
            var address = deliveryAddress.Address;

            if (_deliveryService.CheckIfAddressExist(address))
            {
                delivery.AddressId =
                    _deliveryService.GetIdAddressIfExists(address).Value;
                _deliveryService.AddDeliveryToDb(delivery);
            }
            else
            {
                _deliveryService.AddAddressToDb(address);
                delivery.AddressId = _deliveryService.GetIdAddressIfExists(address).Value;
                _deliveryService.AddDeliveryToDb(delivery);
            }

            _deliveryService.UpdateOrder
                (deliveryAddress.IdOrder, delivery.Id, delivery.DeliveryTypeId);
        }

        private SelectList GetDeliveryTypesList(IEnumerable<DeliveryType> deliveryTypes)
        {
            return new SelectList(deliveryTypes,"Id","FullInfo", "Payment",1);
        }
    }
}