﻿using Warehouse.Domain.Abstract;
using Warehouse.WebUI.Models;
using Warehouse.WebUI.Security;

namespace Warehouse.WebUI.Services.Client
{
    public class ClientService
    {
        private readonly IRepository<Domain.Entities.Client> _clientsRepository;
        private string _clientHashValue = string.Empty;
        private byte[] _clientSalt = new byte[Hasher.saltLengthLimit];

        public ClientService(IRepository<Domain.Entities.Client> clientsRepository)
        {
            _clientsRepository = clientsRepository;
        }

        public bool ValidateLogin(string login)
        {
            return _clientsRepository.GetOne(x => x.Login == login) == null;
        }

        public bool ValidateEmail(string email)
        {
            return _clientsRepository.GetOne(x => x.Email == email) == null;
        }

        public bool ValidatePesel(string pesel)
        {
            if (pesel == null)
                return true;

            return _clientsRepository.GetOne(x => x.Pesel == pesel) == null;
        }

        public bool ValidateNip(string nip)
        {
            if (nip == null)
                return true;

            return _clientsRepository.GetOne(x => x.Nip == nip) == null;
        }

        public bool ValidateLogging(ClientVML clientVML)
        {
            Domain.Entities.Client client = Get(clientVML.Login);
            if (client == null) return false;
            _clientHashValue = client.Hash;
            _clientSalt = client.Salt;

            return Hasher.CompareHashValue(clientVML.Password, 
                clientVML.Login, _clientHashValue, _clientSalt);
        }

        public void AddClientToDb(Domain.Entities.Client client)
        {
            _clientsRepository.Add(client);
        }

        public Domain.Entities.Client Get(string login)
        {
            return _clientsRepository.GetOne(x => x.Login == login);
        }

        public int GetId(string login)
        {
            return _clientsRepository.GetOne(x => x.Login == login).Id;
        }

        public Domain.Entities.Client HashAndSaltClient(ClientVMR user)
        {
            byte[] salt = Hasher.Get_SALT();
            string hash = Hasher.Get_HASH_SHA512(user.Password, user.Client.Login, salt);

            Domain.Entities.Client client = user.Client;
            client.Salt = salt;
            client.Hash = hash;

            return client;
        }
    }
}