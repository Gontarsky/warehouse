﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Services.Order
{
    public class OrderService
    {
        private IRepository<Domain.Entities.Order> _orderRepository;
        private IRepository<Domain.Entities.Product> _productRepository;

        public OrderService(IRepository<Domain.Entities.Order> orderRepository, IRepository<Domain.Entities.Product> productRepository)
        {
            _orderRepository = orderRepository;
            _productRepository = productRepository;
        }

        public Domain.Entities.Order GetOrder(int id)
        {
            return _orderRepository.Get(id);
        }

        public Domain.Entities.Product GetProduct(int id)
        {
            return _productRepository.Get(id);
        }

        public void DeleteOrderFromDb(Domain.Entities.Order order)
        {
            _orderRepository.Delete(order);
        }

        public void InitializeOrder(int idClient, Domain.Entities.Order order)
        {
            order.ClientId = idClient;
            order.OrderDate = DateTime.Now;
            order.GrossOrderValue = order.CalculateGrossValueOrder();
            order.NetOrderValue = order.CalculateNetValueOrder();
            order.Status = Status.Pending;
            order.Payment = Payment.Undefined;
        }

        public void AddOrderToDb(Domain.Entities.Order order)
        {
            _orderRepository.Add(order);
        }
    }
}