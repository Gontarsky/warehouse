﻿using System.Linq;
using Warehouse.Domain.Abstract;
using Warehouse.WebUI.Models.Pagination;

namespace Warehouse.WebUI.Services.Order
{
    public class PagginationService
    {
        private IRepository<Domain.Entities.Order> _orderRepository;

        public PagginationService(IRepository<Domain.Entities.Order> orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public EntitiesListViewModel<Domain.Entities.Order> 
                            GetPagginationModelForClient(int idClient, int page, int pageSize)
        {
            EntitiesListViewModel<Domain.Entities.Order> model = new EntitiesListViewModel<Domain.Entities.Order>
            {
                Entities = _orderRepository.Get(x => x.ClientId == idClient).OrderByDescending(x => x.OrderDate)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = _orderRepository.Get(null).Count()
                }
            };

            return model;
        }
    }
}