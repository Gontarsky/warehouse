﻿using System.IO;
using System.Net.Mail;
using Warehouse.WebUI.Abstract;
using Warehouse.WebUI.Models;

namespace Warehouse.WebUI.Services.Email
{
    public class EmailService : IEmailSender
    {
        public void SendEmail(EmailModel emailModel)
        {
            var mailMessage = new MailMessage();
            mailMessage.To.Add(emailModel.Email);
            mailMessage.Subject = emailModel.Subject;
            mailMessage.Body = emailModel.Message;

            if(emailModel.Attachment != null && emailModel.Attachment.ContentLength > 0)
                mailMessage.Attachments.Add(new Attachment(emailModel.Attachment.InputStream,Path.GetFileName(emailModel.Attachment.FileName)));

            using (var smtp = new SmtpClient())
            {
                smtp.Send(mailMessage);
            }
        }

        public void SendEmailForContact(EmailModel emailModel)
        {
            var body = "<p>Email Od: {0} {1} ({2})</p><p>Wiadomość:</p><p>{3}</p>";
            var mailMessage = new MailMessage();
            mailMessage.To.Add("warehouse.worldbox@gmail.com");
            mailMessage.Subject = emailModel.Subject;
            mailMessage.Body = string.Format(body,emailModel.FirstName,
                emailModel.Lastname,emailModel.Email,emailModel.Message);
            mailMessage.IsBodyHtml = true;

            if (emailModel.Attachment != null && emailModel.Attachment.ContentLength > 0)
                mailMessage.Attachments.Add(new Attachment(emailModel.Attachment.InputStream, Path.GetFileName(emailModel.Attachment.FileName)));

            using (var smtp = new SmtpClient())
            {
                smtp.Send(mailMessage);
            }
        }
    }
}