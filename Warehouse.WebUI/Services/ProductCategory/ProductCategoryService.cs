﻿using System.Collections.Generic;
using System.Linq;
using Warehouse.Domain.Abstract;

namespace Warehouse.WebUI.Services.ProductCategory
{
    public class ProductCategoryService
    {
        private IRepository<Domain.Entities.ProductCategory> _productCategoryRepository;

        public ProductCategoryService(IRepository<Domain.Entities.ProductCategory> productCategoryRepository)
        {
            _productCategoryRepository = productCategoryRepository;
        }

        public List<Domain.Entities.ProductCategory> GetProductCategories()
        {
            return _productCategoryRepository.Get().ToList();
        }
    }
}