﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Services.Product
{
    public class ProductService
    {
        public double MaxSizeOfPictureInkB { get; set; }
        public double MaxSizeOfPictureInMB { get; set; }
        private const double _sizeOfkB = 1024;
        private IRepository<Domain.Entities.Product> _productRepository;
        private IRepository<ProductDetails> _productDetails;

        public ProductService(IRepository<Domain.Entities.Product> productRepository, IRepository<ProductDetails> productDetails)
        {
            _productRepository = productRepository;
            _productDetails = productDetails;
            MaxSizeOfPictureInkB = 5000 * _sizeOfkB; // 5Mb
            MaxSizeOfPictureInMB = MaxSizeOfPictureInkB / (1000 * _sizeOfkB);
        }

        public bool ValidateTypeFile(HttpPostedFileBase file)
        {
            if (file != null && !file.ContentType.Contains("image"))
            {
                return false;
            }

            return true;
        }

        public bool ValidateSizeFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > MaxSizeOfPictureInkB)
            {
                return false;
            }

            return true;
        }

        public ProductDetails GetProductDetails(int productId, string relativePathProduct)
        {
            ProductDetails productDetails = new ProductDetails()
            {
                PathPhoto = relativePathProduct,
                ProductId = productId
            };

            return productDetails;
        }

        public void SaveFile(HttpPostedFileBase file, string combinedPath)
        { 
            file.SaveAs(combinedPath + file.FileName);
        }

        public void AddProductToDb(Domain.Entities.Product product)
        {
            _productRepository.Add(product);
        }

        public void AddProductDetailsToDb(ProductDetails productDetails)
        {
            _productDetails.Add(productDetails);
        }

        public Domain.Entities.Product GetProduct(int id)
        {
            return _productRepository.Get(id);
        }

        public void ConnectProductsWithImages(List<Domain.Entities.Product> products)
        {
            foreach (var product in products)
            {
                var tempId = product.Id;
                product.ProductDetailses = _productDetails.Get(x => x.ProductId == tempId).ToList();
            }
        }


    }
}