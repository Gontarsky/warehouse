﻿using System.Linq;
using Warehouse.Domain.Abstract;
using Warehouse.WebUI.Models.Pagination;

namespace Warehouse.WebUI.Services.Product
{
    public class PagginationService
    {
        private IRepository<Domain.Entities.Product> _productRepository;

        public PagginationService(IRepository<Domain.Entities.Product> productRepository)
        {
            _productRepository = productRepository;
        }

        public EntitiesListViewModel<Domain.Entities.Product> GetPagginationModel(int page, int pageSize,string category = null)
        {
            EntitiesListViewModel<Domain.Entities.Product> model = new EntitiesListViewModel<Domain.Entities.Product>
            {
                Entities = _productRepository
                    .Get(x => category == null || x.Category.CategoryName == category)
                    .OrderByDescending(x => x.NetProductValue)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = category == null ?
                        _productRepository.Get(null).Count() :
                        _productRepository.Get(x => x.Category.CategoryName == category).Count()
                },
                CurrentCategory = category
            };
            return model;
        }

        public EntitiesListViewModel<Domain.Entities.Product> GetSearchedModel(int page, int pageSize, string phrase)
        {
            EntitiesListViewModel<Domain.Entities.Product> model = new EntitiesListViewModel<Domain.Entities.Product>
            {
                Entities = _productRepository
                    .Get(x => phrase == null || x.Name.Contains(phrase))
                    .OrderByDescending(x => x.NetProductValue)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize).ToList(),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = phrase == null ?
                        _productRepository.Get(null).Count() :
                        _productRepository.Get(x => x.Name.Contains(phrase)).Count()
                }
            };
            return model;
        }


    }
}