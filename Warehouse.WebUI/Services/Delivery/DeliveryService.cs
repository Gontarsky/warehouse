﻿using System.Collections.Generic;
using Warehouse.Domain.Abstract;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Models;

namespace Warehouse.WebUI.Services.Delivery
{
    public class DeliveryService
    {
        private IRepository<Domain.Entities.Delivery> _deliveryRepository;
        private IRepository<Domain.Entities.Order> _orderRepository;
        private IRepository<DeliveryType> _deliveryTypeRepository;
        private IRepository<Address> _addressRepository;

        public DeliveryService(IRepository<Domain.Entities.Delivery> deliveryRepository, IRepository<Domain.Entities.Order> orderRepository, IRepository<DeliveryType> deliveryTypeRepository, IRepository<Address> addressRepository)
        {
            _deliveryRepository = deliveryRepository;
            _orderRepository = orderRepository;
            _deliveryTypeRepository = deliveryTypeRepository;
            _addressRepository = addressRepository;
        }

        public DeliveryAddressViewModel GetDeliveryAddressVM(int idOrder, string selectedType)
        {
            var data = new DeliveryAddressViewModel
            {
                IdOrder = idOrder,
                SelectedType = selectedType,
                Address = new Address(),
                Delivery = new Domain.Entities.Delivery()
            };

            return data;
        }

        public IEnumerable<DeliveryType> GetTypesOfDelivery()
        {
            return _deliveryTypeRepository.Get();
        }

        public bool CheckIfWasDone(int idOrder)
        {
            var order = _orderRepository.Get(idOrder);
            if (order == null)
                return true;

            return order.DeliveryId != null;
        }

        public bool CheckIfAddressExist(Address address)
        {
            if (GetIdAddressIfExists(address) == null)
                return false;

            return true;
        }

        public bool CheckIfOrderHasDelivery(int orderId)
        {
            if (_orderRepository.Get(orderId)?.DeliveryId == null) 
                return false;

            return true;
        }

        public DeliveryTypeViewModel GetDeliveryTypeVM(int orderId)
        {
            Domain.Entities.Order order = _orderRepository.Get(orderId);
            Domain.Entities.Delivery delivery = _deliveryRepository.Get(order.DeliveryId.Value);
            DeliveryType deliveryType = _deliveryTypeRepository.Get(delivery.DeliveryTypeId);
            Address address = _addressRepository.Get(delivery.AddressId);

            var deliveryTypeVM = new DeliveryTypeViewModel
            {
                Delivery = delivery,
                DeliveryType = deliveryType,
                Address = address
            };

            return deliveryTypeVM;
        }

        public int? GetIdAddressIfExists(Address address)
        {
            var existAddress = _addressRepository.GetOne(x => x.Postcode == address.Postcode &&
                                                              x.City == address.City &&
                                                              x.Street == address.Street &&
                                                              x.LocalNumber == address.LocalNumber);
            return existAddress?.Id;
        }

        public void AddDeliveryToDb(Domain.Entities.Delivery delivery)
        {
            _deliveryRepository.Add(delivery);
        }

        public void AddAddressToDb(Address address)
        {
            _addressRepository.Add(address);
        }

        public void UpdateOrder(int idOrder, int idDelivery, int idDeliveryType)
        {
            var deliveryType = _deliveryTypeRepository.Get(idDeliveryType);

            var order = _orderRepository.Get(idOrder);
            order.DeliveryId = idDelivery;
            order.Payment = deliveryType.Payment;
            order.NetOrderValue += deliveryType.CostOfDeliveryType;
            order.GrossOrderValue = order.CalculateGrossValueOrder();
            _orderRepository.Update(order);
        }

        public Domain.Entities.Order GetOrder(int id)
        {
            return _orderRepository.Get(id);
        }

        public Domain.Entities.Delivery GetDelivery(int id)
        {
            return _deliveryRepository.Get(id);
        }

        public DeliveryType GetDeliveryType(int id)
        {
            return _deliveryTypeRepository.Get(id);
        }

        public Address GetAddress(int id)
        {
            return _addressRepository.Get(id);
        }


    }
}

