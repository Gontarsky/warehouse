﻿using Warehouse.Domain.Abstract;
using Warehouse.WebUI.Models;

namespace Warehouse.WebUI.Services.Home
{
    public class HomeService
    {
        private IRepository<Domain.Entities.Client> _clientRepository;

        public HomeService(IRepository<Domain.Entities.Client> clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public Domain.Entities.Client GetClient(int id)
        {
            return _clientRepository.Get(id);
        }

        public EmailModel GetEmailModelForClient(int idClient)
        {
            var currentClient = GetClient(idClient);
            var emailModel = new EmailModel()
            {
                FirstName = currentClient.FirstName,
                Lastname = currentClient.LastName,
                Email = currentClient.Email
            };

            return emailModel;
        }
    }
}