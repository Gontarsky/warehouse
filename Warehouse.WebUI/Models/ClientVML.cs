﻿using System.ComponentModel.DataAnnotations;

namespace Warehouse.WebUI.Models
{
    public class ClientVML
    {
        [Required(ErrorMessage = "Login jest wymagany do procesu uwierzytelnienia.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane do procesu uwierzytelnienia.")]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }


        public string ReturnUrl { get; set; }
        [Display(Name= "Zapamiętaj")]
        public bool IsRemember { get; set; }
    }
}