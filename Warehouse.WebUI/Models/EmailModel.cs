﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Warehouse.WebUI.Models
{
    public class EmailModel
    {
        [Required(ErrorMessage = "Imię jest wymagane.")]
        [Display(Name = "Imię")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Nazwisko jest wymagane.")]
        [Display(Name = "Nazwisko")]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Email jest wymagany.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Temat jest wymagany.")]
        [Display(Name = "Temat")]
        public string Subject { get; set; }
        [Required(ErrorMessage = "Treść email'a jest wymagana.")]
        [Display(Name = "Treść")]
        public string Message { get; set; }
        public HttpPostedFileBase Attachment { get; set; }
    }
}