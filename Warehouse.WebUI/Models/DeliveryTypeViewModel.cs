﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Models
{
    public class DeliveryTypeViewModel
    {
        public Delivery Delivery { get; set; }
        public DeliveryType DeliveryType { get; set; }
        public Address Address { get; set; }
    }
}