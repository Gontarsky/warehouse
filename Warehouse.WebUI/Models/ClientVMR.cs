﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Models
{
    public class ClientVMR
    {
        public Client Client { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagany do procesu uwierzytelnienia.")]
        [StringLength(50, ErrorMessage = "{0} musi mieć przynajmniej {2} znaków.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Hasło")]
        public string Password { get; set; }


        public string ReturnUrl { get; set; }

        public bool isRemember { get; set; }
    }
}