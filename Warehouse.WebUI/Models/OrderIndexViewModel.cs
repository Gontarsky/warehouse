﻿using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Models
{
    public class OrderIndexViewModel
    {
        public Order Order { get; set; }
        public string ReturnUrl { get; set; }
    }
}