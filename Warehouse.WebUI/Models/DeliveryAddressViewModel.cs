﻿using System.ComponentModel.DataAnnotations;
using Warehouse.Domain.Entities;

namespace Warehouse.WebUI.Models
{
    public class DeliveryAddressViewModel
    {
        public Delivery Delivery { get; set; }
        public Address Address { get; set; }
        [MaxLength(100)]
        [DataType(DataType.MultilineText)]
        public string SelectedType { get; set; }
        public int IdOrder { get; set; }
    }
}