﻿using System.Collections.Generic;

namespace Warehouse.WebUI.Models.Pagination
{
    public class EntitiesListViewModel<TEntity>
    {
        public List<TEntity> Entities { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}