﻿using System.Web;
using System.Web.Mvc;

namespace Warehouse.WebUI.Security
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if(HttpContext.Current.Session["ClientId"] == null)
            {
                filterContext.Result = new RedirectResult("~/Client/Login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}