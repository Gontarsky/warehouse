﻿using System.Web.Mvc;
using System.Web.Routing;
using Warehouse.Domain.Entities;
using Warehouse.WebUI.Infrastructure.Binders;

namespace Warehouse.WebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            ModelBinders.Binders.Add(typeof(Order), new OrderModelBinder());
        }
    }
}
