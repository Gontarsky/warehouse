﻿using System.Web.ModelBinding;
using System.Web.Mvc;
using Warehouse.Domain.Entities;
using IModelBinder = System.Web.Mvc.IModelBinder;
using ModelBindingContext = System.Web.Mvc.ModelBindingContext;

namespace Warehouse.WebUI.Infrastructure.Binders
{
    public class OrderModelBinder : IModelBinder
    {
        private const string sessionKey = "Order";


        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Order order = null;

            if (controllerContext.HttpContext.Session != null)
                order = (Order) controllerContext.HttpContext.Session[sessionKey];

            if (order == null)
            {
                order = new Order();
                if (controllerContext.HttpContext.Session != null)
                    controllerContext.HttpContext.Session[sessionKey] = order;
            }

            return order;
        }
    }
}